package com.a1633867.loancalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calculate(View view) {
        EditText years = (EditText)findViewById(R.id.years);
        EditText loan = (EditText)findViewById(R.id.loanAmount);
        EditText interest = (EditText)findViewById(R.id.interestRate);

        if (years.getText().toString().equals("") || loan.getText().toString().equals("") || interest.getText().toString().equals("") ) {
            return;
        }

        int numberOfYears = Integer.parseInt(years.getText().toString());
        double loanAmount = Double.parseDouble(loan.getText().toString());
        double interestRate = Double.parseDouble(interest.getText().toString());

        LoanCalculator lc = new LoanCalculator(loanAmount, numberOfYears, interestRate);
        this.setResults(lc);
    }

    public void reset(View view) {
        EditText[] etArray = {(EditText)findViewById(R.id.years) , (EditText)findViewById(R.id.loanAmount) ,(EditText)findViewById(R.id.interestRate)};
        TextView[] tvArray = { (TextView)findViewById(R.id.monthlyPay),(TextView)findViewById(R.id.totalPay) ,(TextView)findViewById(R.id.totalInterest)};
        for (int i = 0; i < etArray.length; i++) {
            etArray[i].setText("");
            tvArray[i].setText("0");
        }

    }

    private void setResults(LoanCalculator lc) {
        TextView monthlyPay = (TextView)findViewById(R.id.monthlyPay);
        TextView totalPay = (TextView)findViewById(R.id.totalPay);
        TextView totalInterest = (TextView)findViewById(R.id.totalInterest);

        monthlyPay.setText(Double.toString(lc.getMonthlyPayment()));
        totalPay.setText(Double.toString(lc.getTotalCostOfLoan()));
        totalInterest.setText(Double.toString(lc.getTotalInterest()));
    }

}
